import Graphics.Gloss

alterne :: [a] -> [a]
alterne [] = []
alterne [x] = [x]
alterne (x1:x2:xs) = x1 : alterne xs

combine :: (a -> b -> c) -> [a] -> [b] -> [c]
combine f (x:xs) (y:ys) = f x y : combine f xs ys
combine _ _ _ = []

pasPascal :: [Integer] -> [Integer]
pasPascal xs = zipWith (+) (0:xs) (xs ++ [0])

pascal :: [[Integer]]
pascal = take 10 (iterate (pasPascal) [1])


pointAintercaler :: Point -> Point -> Point
pointAintercaler x y = ( ((fst x) + fst(y))/2 + ( snd(y) - snd(x) )/2, (snd(x)+ snd(y))/2 + (fst(x) - fst(y))/2 ) 

pasDragon :: Path -> Path
pasDragon [] = []
pasDragon (x:y:z:xs) = x : pointAintercaler x y : y : pointAintercaler z y : pasDragon(z : xs)
pasDragon [x, y] = x : pointAintercaler x y : y : []

dragon :: Point -> Point -> [Path]
dragon 
