sommeDeXaY :: Int -> Int -> Int
sommeDeXaY x y 
    | x > y = 0
    | otherwise = x  + sommeDeXaY (x+1) y


somme :: [Int] -> Int
somme  [] = 0
somme (x:xs) = x + somme xs

last' :: [a] -> a
last' [] = error "liste vide"
last' xs = head (reverse xs)

init' :: [a] -> [a]
init' [] = error "liste vide"
init' xs = take ( (length xs) - 1) xs

enieme :: [a] -> Int -> a
enieme [] n = error "liste vide"
enieme (x : xs) 0 = x
enieme (x : xs) n = enieme xs (n-1)

plusplus :: [a] -> [a] -> [a]
plusplus _ [] = error "liste vide"
plusplus xs (y:ys) = xs

concat' :: [[a]] -> [a]
concat' [] = []
concat' (x:xss) = x ++ concat' xss

map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

longueurListe :: (Num b) => [a] -> b
longueurListe [] = 0



